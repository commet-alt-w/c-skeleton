#include <stdlib.h> 
#include <stdio.h>
#include <syslog.h>

int main() {
  printf("hello, world!\n");
  
#ifdef DEBUG
  syslog(LOG_DEBUG, "debug logging");
#endif

  exit(EXIT_SUCCESS);
}

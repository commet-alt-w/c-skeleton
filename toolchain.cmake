#toolchain.cmake
# cmake toolchain file

# c compiler and standard
set(CMAKE_C_COMPILER "/usr/bin/gcc")
set(CMAKE_C_STANDARD "11")
set(CMAKE_C_STANDARD_REQUIRED YES)

# global cflags
set(CMAKE_C_FLAGS "-Wall -Wextra -Wunreachable-code -Wpedantic"
  CACHE STRING "cflags")

# cflags for debug build
set(CMAKE_C_FLAGS_DEBUG "-O0 -D_DEBUG -g3 -ggdb3"
  CACHE STRING "debug cflags")

# cflags for release build
set(CMAKE_C_FLAGS_RELEASE "-O2 -g -DNDEBUG -s"
  CACHE STRING "release cflags")

# linker flags
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,--strip-debug"
  CACHE STRING "release linker flags")
